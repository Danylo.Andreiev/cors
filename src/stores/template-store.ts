// import { AxiosResponse } from 'axios';
import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
// import { useUserStore } from './user-store';

export interface ITaskTemplate {
  id: string;
  name: string;
  parent: string;
  sheduleName: string;
  typeOfWorkID: string;
  typeOfWorkName: string;
  dateInName: boolean;
  description: string;
  storyPoint: number;
  storyPointPlan: number;
  storyPointClient: number;  
  daysPerform: number;
  shedule: {
    beginTime: string;
    endTime: string;
    сompletionTime: string;
    beginDate: string;
    endDate: string;
    dayInMonth: number;
    weekDayInMonth: number;
    weekDays: Array<string>;
    months: Array<string>;
    weeksPeriod: number;
    repeatPeriodInDay: number;
    daysRepeatPeriod: number;
    pauseRepeat: number;
    completionInterval: number;
  };
}

export const useTaskTemplateStore = defineStore('taskTemplates', {
  state: () => ({
    taskTemplates: [] as ITaskTemplate[],
  }),

  actions: {
    async getTaskTemplates() {
      // const res: AxiosResponse<ITaskTemplate[]> = await api.get('');

      // await api.get(`?page=${currentPage}&pageSize=${pageSize}&${find}`)
      const res: string = await api.get('')

      // if (!res.data) return;
      // this.taskTemplates = res.data;
      return res;
    },

  },

  getters: {

    getColumnsById: (state) => (columnId: string) => {
      const taskTemplates = state.taskTemplates.filter((task) => task.typeOfWorkID === columnId);
      return taskTemplates;
    },

    getColumns: (state) => {
      const taskTemplates = state.taskTemplates;
      const columns = taskTemplates
        .map((task) => ({ id: task.typeOfWorkID, name: task.typeOfWorkName }))
        .sort((a, b) => a.id.localeCompare(b.id));
      const columnsToReturn = [] as { id: string; name: string }[];
      columns.forEach(({ id, name }) => {
        if (columnsToReturn.map((column) => column.id).includes(id)) return;
        columnsToReturn.push({ id, name });
      });
      return columnsToReturn;
    },

  }
});
