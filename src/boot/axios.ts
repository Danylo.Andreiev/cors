import { boot } from 'quasar/wrappers';
import axios, { AxiosInstance } from 'axios';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
    $api: AxiosInstance;
  }
}

const login = 'CRM';
const password = 'CRM';
const basicAuthCredentials = btoa(`${login}:${password}`);

const api = axios.create({ 
  baseURL: 'http://mobileapp.msht.eu:9696/TestHTTP/hs/CRM',
  withCredentials: true,
  headers: {
    'Authorization': `Basic ${basicAuthCredentials}`,
  },
});

export default boot(({ app }) => {
  app.config.globalProperties.$axios = axios;
  app.config.globalProperties.$api = api;
});

export { api };
